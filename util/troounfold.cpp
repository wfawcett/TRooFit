
#include "TRooFit/TRooWorkspace.h"
#include "TRooFit/Utils.h"
#include <iostream>

#include "TEnv.h"
#include "TFile.h"
#include "TApplication.h"
#include "TCanvas.h"

std::vector<TString> dirKeys(TDirectory* d, const char* obj_type) {
    std::vector<TString> out;
    auto keys = d->GetListOfKeys();
    for(int i=0;i < keys->GetEntries(); i++) {
      TKey* k = (TKey*)keys->At(i);
      if (!TClass::GetClass(k->GetClassName())->InheritsFrom(obj_type)) continue;
      out.push_back( k->GetName() );
    }
    return out;
}



int main(int argc, char* argv[]) {
 
  TApplication *app = new TApplication("app", 0, 0);
  
  if (argc < 2) {
    std::cout << "ERROR: Please provide an input or config file" << std::endl;
    return -1;
  }
  
  TEnv e;
  e.SetValue("InputFile", argv[1]);
  e.SetValue("HistSuffix", "MET");
  e.SetValue("BinByBin", true );
  
  if (!e.Defined("ResponseMatrixName")) {
    e.SetValue("ResponseMatrixName",Form("response%s",e.GetValue("HistSuffix",(char*)0)));
  }
  
  if (!e.Defined("DataName")) {
    e.SetValue("DataName",Form("data%s",e.GetValue("HistSuffix",(char*)0)));
  }
  
  if (!e.Defined("TruthPrefix")) {
    e.SetValue("TruthPrefix","truth"); 
  }
  
  if (!e.Defined("POIPrefix")) {
    e.SetValue("POIPrefix","yield"); 
  }
  
  const char* input_file = e.GetValue("InputFile",(char*)0);  
  TFile f(input_file);
  
  // find the response and data objects
  TH2* response = (TH2*)f.Get(e.GetValue("ResponseMatrixName",(char*)0));
  if (response == nullptr) {
    std::cout << "ERROR: Could not find " << e.GetValue("ResponseMatrixName",(char*)0) << std::endl;
    return -1;
  }
  
  TH1* data = (TH1*)f.Get(e.GetValue("DataName",(char*)0));
  if (data == nullptr) {
    std::cout << "ERROR: Could not find " << e.GetValue("DataName",(char*)0) << std::endl;
    return -1;
  }
  
  // find hist beginning with TruthPrefix, and use that to determine signal name
  auto hists = dirKeys(&f,"TH1");
  TString sig_name;
  for (auto& h : hists) {
   if ( !h.BeginsWith(e.GetValue("TruthPrefix",(char*)0)) ) continue;
   int ii = strlen(e.GetValue("TruthPrefix",(char*)0));
   sig_name = h(ii,h.Length()-strlen(e.GetValue("HistSuffix",(char*)0))-ii);
   break;
  }
  
  if (sig_name.Length() == 0) {
   std::cout << "ERROR: Could not find truth-level signal distribution" << std::endl; 
   return -1;
  }
  
  std::cout << "INFO: Detected signal component name: " << sig_name << std::endl;
  
  TH1* truth = (TH1*)f.Get(Form("%s%s%s",
				e.GetValue("TruthPrefix",(char*)0),
				sig_name.Data(),
				e.GetValue("HistSuffix",(char*)0)) );
  
  if (!truth) {
      std::cout << "ERROR: Could not load truth histogram" << std::endl;
      return -1;
  }
  
  if (strlen(truth->GetTitle())==0) {
      truth->SetTitle(sig_name);
  }
  
  TString reco_name = Form("%s%s",
				sig_name.Data(),
				e.GetValue("HistSuffix",(char*)0));
  TH1* reco = (TH1*)f.Get(reco_name);
  
  if (!reco) {
      std::cout << "WARNING: Could not find reco histogram - inferring from response matrix" << std::endl;
      reco = response->ProjectionY(reco_name);
  }

  
  TRooWorkspace* w = new TRooWorkspace("troounfold");
  
  // use the response matrix to give title to observable
  TString r_title = response->GetYaxis()->GetTitle();
  if (r_title.Length() == 0) {
     std::cout << "INFO: No title on response matrix y-axis (reco) - using r_{rec}" << std::endl;
     r_title = "r_{rec}";
  }
  w->addObservable("r_rec",r_title,response->GetNbinsY(),response->GetYaxis()->GetXbins()->GetArray());

  // systematics are TDirectory in the input file
  auto systs = dirKeys(&f, "TDirectory");
  if (!systs.empty()) {
      auto systs_copy = systs;
      systs.clear();
      for(auto& s : systs_copy) {
	if (s.EndsWith("_DOWN")) continue;
	else if(s.EndsWith("_UP")) {
	  s = s(0,s.Length()-3);
	}
	systs.push_back(s);
      }
      std::cout << "INFO: Detected " << systs.size() << " systematics" << std::endl;
      for (auto syst : systs) {
	w->addParameter(Form("alpha_%s",syst.Data()),syst,0,-5,5,"NORMAL");
      }
  }
  
  
  w->addChannel("SR","Signal Region","r_rec");
  
  // loop over hists in the file, anything that isnt response or data or truth, add as bkg
  std::vector<TString> bkg_names;
  
  std::cout << "INFO: Detecing background components ... " << std::endl;
  for(auto h : hists) {
    if ( h == response->GetName() ) continue;
    if ( h == data->GetName() ) continue;
    if ( h == truth->GetName() ) continue;
    if ( h == reco->GetName() ) continue;
    TH1* b = (TH1*)f.Get(h);
    if (b->GetDimension() > 1) {
      std::cout << "WARNING: Skipping " << h << " wrong dimension" << std::endl;
      continue;
    }
    
    // got here, so assume is a bkg component, create and add it!
    TString b_name = h(0, h.Length() - strlen(e.GetValue("HistSuffix",(char*)0)));
    bkg_names.push_back( b_name );
    
    w->addSamples(b_name,b->GetTitle(),"SR");
    w->Add(b_name,"SR",b);
   
    // loop over systematics, checking for variation
    for(auto& syst : systs) {
	bool isUp = false;
	TH1* variation = (TH1*)f.Get(Form("%s/%s",syst.Data(),h.Data()));
	if (!variation) {
	  // try _UP variation
	  isUp=true;
	  variation = (TH1*)f.Get(Form("%s_UP/%s",syst.Data(),h.Data()));
	  if (!variation) continue;
	}
	std::cout << "  INFO: background " << b_name << " has systematic " << syst << std::endl;
	w->Add(b_name,"SR",variation,Form("alpha_%s",syst.Data()),1);
	if (isUp) {
	    //try the down variation
	    variation = (TH1*)f.Get(Form("%s_DOWN/%s",syst.Data(),h.Data()));
	    if (variation) w->Add(b_name,"SR",variation,Form("alpha_%s",syst.Data()),-1);
	}
    }
    
  }
  
  if (!e.GetValue("BinByBin", (int)0)) {
      std::cout << "INFO: Checking for out-of-fiducial contamination ... " << std::endl;
      TH1* reco_from_truth = response->ProjectionY("_py",1,response->GetNbinsX());
      reco_from_truth->Divide(reco); // this is now the fiducial purity in each reco bin
      // check if any purity is not equal to 1
      //check if any of the bins are > 1 ... which would indicate a problem ..
      if(reco_from_truth->GetBinContent( reco_from_truth->GetMaximumBin() ) > 1.000001) {
        printf("Fiducial Purity > 1 (%f) found in bin %d", reco_from_truth->GetBinContent( reco_from_truth->GetMaximumBin() ), reco_from_truth->GetMaximumBin());
        return -1;
      }
      
      //need to register inverse purity as a scale factor ...
      TH1* fidCorrection = (TH1*)reco_from_truth->Clone("fidCorrection");
      fidCorrection->Reset();
      for(int i=1;i<=fidCorrection->GetNbinsX();i++) { fidCorrection->SetBinContent(i,1.); }
      fidCorrection->Divide(reco_from_truth); //is now the inverse purity, add as a scale factor on all the signal components 
      //only do so if the invPurity in any bin is sufficiently different from 1 
      bool nonUnity(false);
      for(int i=1;i<=fidCorrection->GetNbinsX();i++) { if(fabs(fidCorrection->GetBinContent(i)-1.)>1e-5) {nonUnity=true; break; } }
      if(nonUnity) {
	
	 std::cout << "Adding out-of-fiducial contamination as a background component" << std::endl;
	 TH1* out_of_volume = (TH1*)reco->Clone("out_of_volume");
	 for (int i=1;i<=out_of_volume->GetNbinsX();i++) {
	  out_of_volume->SetBinContent(i,out_of_volume->GetBinContent(i)-reco_from_truth->GetBinContent(i));
	  out_of_volume->SetBinError(i,sqrt(pow(out_of_volume->GetBinError(i),2)-pow(reco_from_truth->GetBinError(i),2)));
	 }
	 
	 w->addSamples("out_of_volume","Out of volume","SR");
	 w->Add("out_of_volume","SR",out_of_volume);
	 
      }
  }
  
  std::cout << "INFO: Creating efficiency histograms ... " << std::endl;
  // now construct the efficiency histograms for each bin from response matrix and truth histogram
  for(int i=1; i <= response->GetNbinsX(); i++) {
      TString c_name = Form("eff%d",i);
      w->addSamples(c_name,Form("%s[%g-%g]",truth->GetTitle(),
				response->GetXaxis()->GetBinLowEdge(i),
				response->GetXaxis()->GetBinUpEdge(i)),
		    "SR");
      
      TH1* eff = response->ProjectionY(c_name,i,i);
      if (e.GetValue("BinByBin", (int)0)) {
	// doing bin-by-bin unfolding - no bin migration
	eff->Reset();
	eff->SetBinContent(i, reco->GetBinContent(i) );
      }
      eff->Scale(1.0 / truth->GetBinContent(i) );
      
      w->Add(c_name,"SR", eff);
      
      // create parameter of interest and scale
      TString p_name = Form("%s%d",e.GetValue("POIPrefix",(char*)0),i);
      w->addParameter(p_name, p_name, truth->GetBinContent(i), 0, truth->GetBinContent(i)*10 );
      w->Scale(c_name,"SR", p_name);
      
      // loop over systematics, checking for variation
      for(auto& syst : systs) {
	  bool isUp = false;
	  TH2* variation = (TH2*)f.Get(Form("%s/%s",syst.Data(),response->GetName()));
	  TH2* vreco = (TH2*)f.Get(Form("%s/%s",syst.Data(),reco->GetName()));
	  TH2* vtruth = (TH2*)f.Get(Form("%s/%s",syst.Data(),truth->GetName()));
	  if (!variation) {
	    // try _UP variation
	    isUp=true;
	    variation = (TH2*)f.Get(Form("%s_UP/%s",syst.Data(),response->GetName()));
	    vreco = (TH2*)f.Get(Form("%s_UP/%s",syst.Data(),reco->GetName()));
	    vtruth = (TH2*)f.Get(Form("%s_UP/%s",syst.Data(),truth->GetName()));
	    if (!variation) continue;
	  }
	  
	  if (i==1) {
	    std::cout << "  INFO: signal has systematic " << syst << std::endl;
	  }
	  
	  TH1* eff = variation->ProjectionY(c_name,i,i);
	  if (e.GetValue("BinByBin", (int)0)) {
	    // doing bin-by-bin unfolding - no bin migration
	    eff->Reset();
	    eff->SetBinContent(i, vreco->GetBinContent(i) );
	  }
	  eff->Scale(1.0 / vtruth->GetBinContent(i) );
	  w->Add(c_name,"SR",eff,Form("alpha_%s",syst.Data()),1);
	  if (isUp) {
	      //try the down variation
	      variation = (TH2*)f.Get(Form("%s_DOWN/%s",syst.Data(),response->GetName()));
	      vreco = (TH2*)f.Get(Form("%s_DOWN/%s",syst.Data(),reco->GetName()));
	      vtruth = (TH2*)f.Get(Form("%s_DOWN/%s",syst.Data(),truth->GetName()));
	      if (variation) {
		TH1* eff = variation->ProjectionY(c_name,i,i);
		if (e.GetValue("BinByBin", (int)0)) {
		  // doing bin-by-bin unfolding - no bin migration
		  eff->Reset();
		  eff->SetBinContent(i, vreco->GetBinContent(i) );
		}
		eff->Scale(1.0 / vtruth->GetBinContent(i) );
		w->Add(c_name,"SR",eff,Form("alpha_%s",syst.Data()),-1);
	      }
	  }
      }
      
  }
  
  // add the data
  w->Add(0, "SR", data);
  
  TCanvas canvas("canvasKey");
  w->Draw();
  canvas.Update();
  
  std::cout << ">>>DOUBLE CLICK ON CANVAS TO CONTINUE<<<" << std::endl;
  canvas.WaitPrimitive();
  
  std::cout << "INFO: Running Fit ..." << std::endl;
  
  w->fitTo();
  
  w->Draw();
  canvas.Update();
  canvas.WaitPrimitive();
  
  w->Print();
  
  //label the floating parameters with a systematic 'group'
  auto floatPars = w->model()->getObservables( w->getFit()->floatParsFinal() ); //a RooArgSet of the actual parameters in the model
  RooFIter itr = floatPars->fwdIterator();
  RooAbsArg* arg = 0;
  RooArgList poi;
  std::cout << "INFO: Assigning parameters to systematic groups ..." << std::endl;
  std::vector<std::string> groups = {"TOTAL","STAT","STATCORR"};
  std::set<TString> found_groups;
  while( (arg = itr.next()) ) {
    TString argName(arg->GetName());
    TString argGroup;
    if(argName.Contains("_stat_")) {
        argGroup = "MCSTAT";
    } else if( argName.Contains("Theory") ) {
        argGroup = "THEORY";
    } else if(argName.BeginsWith(e.GetValue("POIPrefix",(char*)0))) {
	poi.add(*w->var(argName));
        continue; //no group for parameters of interest
    } else if(argName.Contains("_norm_")) {
        argGroup = "NORM";
    } else {
        argGroup = "SYST";
    }
    if (found_groups.find(argGroup) == found_groups.end()) {
      found_groups.insert(argGroup);
      groups.push_back(argGroup.Data());
    }
    std::cout << argName << " -> " << argGroup << std::endl;
    arg->setAttribute(argGroup);
  }
  
  std::cout << "INFO: Performing systematic breakdown ... " << std::endl;
  
  //Now perform the breakdown ... "TOTAL" and "STAT" are special groups that will represent the total error and the error remaining after all other groups are held const
  //std::map<TString,RooArgList*>
  auto bdResult = TRooFit::breakdown( w->getFitNll(), poi, groups); //returns a map from string (group) to errors
  
  int ii=0;
  std::cout << "NAME : VALUE";
  for(auto group : groups) {
   if (ii!=1) std::cout << " +/- ";
   std::cout << group; 
   if (ii==0) std::cout << " (";
   ii++;
  }
  std::cout << ")" << std::endl;
  
  for(int i=1; i <= response->GetNbinsX(); i++) {
      TString p_name = Form("%s%d",e.GetValue("POIPrefix",(char*)0),i);
      std::cout << p_name << " : " << ((RooRealVar*)w->getFit()->floatParsFinal().find(p_name))->getVal();
      int ii = 0;
      for(auto group : groups) {
	if (ii!=1) std::cout << " +/- ";
	std::cout << ((RooRealVar*)bdResult[group]->find(p_name))->getAsymErrorHi() << "/"
			     << ((RooRealVar*)bdResult[group]->find(p_name))->getAsymErrorLo();
	if (ii==0) std::cout << " (";
	ii++;
      }
      std::cout << ")" << std::endl;
  }
  
  return 0;
  
}