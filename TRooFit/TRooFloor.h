//
// Created by Will Buttinger on 20/10/2019.
//
#ifndef TROO_FLOOR
#define TROO_FLOOR

#include "RooAbsReal.h"
#include "RooNumIntConfig.h"

class TRooFloor : public RooAbsReal {

public:
    TRooFloor(const char* name, RooAbsReal& func, double floor=1e-12) :
        RooAbsReal(name,name),
        m_func("func", "function", this, func ),m_floor(floor) {
        specialIntegratorConfig(kTRUE)->method1D().setLabel("RooBinIntegrator");
    }

    TRooFloor(const TRooFloor& other, const char* name = 0) :
        RooAbsReal(other,name),m_func("func",this,other.m_func),m_floor(other.m_floor)
        {

    }

    virtual TObject* clone(const char* newname) const { return new TRooFloor(*this,newname); }

    RooAbsArg::CacheMode canNodeBeCached() const { return RooAbsArg::NotAdvised; }

    virtual Bool_t isBinnedDistribution(const RooArgSet& obs) const {
        return m_func.arg().isBinnedDistribution(obs);
    }

protected:

    virtual double evaluate() const {
        double v = m_func.arg().getVal();
        if (v < m_floor) {
            v = (m_floor - v) + m_floor*(2. / (1. + exp(m_floor - v)) - 1.);
        } else {
            v = 0;
        }
        return v;
    }

private:
    RooRealProxy m_func;
    double m_floor;
    ClassDef(TRooFloor,1) // Flooring function
};

#endif