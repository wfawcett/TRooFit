#ifndef TROOHYPOTESTER
#define TROOHYPOTESTER

/*
  This helper application can be used to do hypothesis testing
  


*/

#include "TRooFit/Utils.h"

#include "TRooFit/TRooWorkspace.h"
#include "Math/BrentRootFinder.h"
#include "Math/WrappedFunction.h"
#include "TGraphErrors.h"
#include "TGraph2DErrors.h"
#include "TMultiGraph.h"
#include "TPad.h"
#include "TLatex.h"
#include "TCanvas.h"
#include "TLegendEntry.h"
#include "TEfficiency.h"
#include "Math/ProbFuncMathCore.h"



//helper struct for finding the test stat value corresponding to a given target p-value (for expected p-values)
struct TailIntegralFunction { 
  TailIntegralFunction( RooRealVar* _poi, double _alt_val, double _sigma_mu, bool (*fPLLTestStatisticType)(double mu, double mu_hat), double _target ) : 
   poi(_poi), alt_val(_alt_val), sigma_mu(_sigma_mu), target(_target), fPLLTestStatisticType(fPLLTestStatisticType)
   {}
  double operator() (double x) const {
    return TRooFit::Asymptotics::altPValue(x,poi,alt_val,sigma_mu,fPLLTestStatisticType) - target;
  }
  RooRealVar* poi;
  double alt_val, sigma_mu, target;
  bool (*fPLLTestStatisticType)(double mu, double mu_hat);
  
};







class TRooHypoTester : public TNamed  {

  public:
    TRooHypoTester(RooWorkspace* _w, const char* poiName, bool (*tsType)(double mu, double mu_hat) = TRooFit::OneSidedPositive) :
      sigma_mu(0),unconditionalFit(0),fPoiName(poiName),fPLLTestStatisticType(tsType) {
      
      Info("TRooHypoTester","Converting RooWorkspace into TRooWorkspace ... Note it is more efficient to do this yourself with code like:");
      Info("TRooHypoTester","TRooWorkspace* w = new TRooWorkspace(*combined); w->writeToFile(\"myWorkspace.root\");");
      
      TRooWorkspace::setDefaultStyle();
      RooMsgService::instance().getStream(RooFit::INFO).removeTopic(RooFit::ObjectHandling); //quiets next line
      w = new TRooWorkspace(*_w);
      
      nullPVals.SetMarkerStyle(20);altPVals.SetMarkerStyle(20);clsPVals.SetMarkerStyle(20);
      nullPVals.SetLineColor(kBlue);altPVals.SetLineColor(kRed);
      
      
      if(fPLLTestStatisticType==TRooFit::OneSidedPositive) SetTitle("Upper Limit Hypothesis Tester");
      else if(fPLLTestStatisticType==TRooFit::OneSidedNegative) SetTitle("Capped Discovery Hypothesis Tester");
      else if(fPLLTestStatisticType==TRooFit::Uncapped) SetTitle("Uncapped Discovery Hypothesis Tester");
      else SetTitle("Custom Hypothesis Tester");
      
    }
  
    TRooHypoTester(TRooWorkspace* _w, const char* poiName, bool (*tsType)(double mu, double mu_hat) = TRooFit::OneSidedPositive) : 
      w(_w),sigma_mu(0),unconditionalFit(0),fPoiName(poiName),fPLLTestStatisticType(tsType)
   { 
    
      nullPVals.SetMarkerStyle(20);altPVals.SetMarkerStyle(20);clsPVals.SetMarkerStyle(20);
      nullPVals.SetLineColor(kBlue);altPVals.SetLineColor(kRed);
      
      
      if(fPLLTestStatisticType==TRooFit::OneSidedPositive) SetTitle("Upper Limit Hypothesis Tester");
      else if(fPLLTestStatisticType==TRooFit::OneSidedNegative) SetTitle("Capped Discovery Hypothesis Tester");
      else if(fPLLTestStatisticType==TRooFit::Uncapped) SetTitle("Uncapped Discovery Hypothesis Tester");
      else SetTitle("Custom Hypothesis Tester");
      
    }
    
    //RooRealVar* poi() { return fWorkspace->var(fPoiName); }
    
    //p-value with error
    /*std::pair<double,double> GetNullPValue(double tsVal) {
      if(kAsymptotics) {
        TRooFit::Asymptotics::nullPValue(tsVal, poi(), double sigma_mu, bool (*fPLLTestStatisticType)(double mu, double mu_hat) ) {
        fCurrentHypoPoint
      }
    }
    
    double GetObservedTestStatVal() {
      
    }*/
    
  ~TRooHypoTester() { 
      if( sigma_mu ) delete sigma_mu; 
      if(unconditionalFit) delete unconditionalFit; 
  }
  
  virtual void Print(Option_t* opt) const override {
    TNamed::Print(opt);
    
    std::cout << "       Parameter of Interest: " << w->var(fPoiName)->GetName() << std::endl;
    std::cout << "  NullHypo (Null hypothesis): " << w->var(fPoiName)->GetName() << " = " << w->var(fPoiName)->getVal() << std::endl;
    std::cout << "  AltHypo  (Alt. hypothesis): " << w->var(fPoiName)->GetName() << " = " << alt_val << std::endl;
    std::cout << "                 Scan Range : " << w->var(fPoiName)->GetName() << " = [" << w->var(fPoiName)->getMin("LimitScan") << " - " << w->var(fPoiName)->getMax("LimitScan") << "]" << std::endl;
    
  }
  
  void SetNullHypo(double in) { w->var(fPoiName)->setVal(in); }
  void SetAltHypo(double in) { alt_val = in; }
  
  
  void reset() { 
    if(sigma_mu) delete sigma_mu; 
    if(unconditionalFit) delete unconditionalFit;
    sigma_mu=0;unconditionalFit = 0;
  }
  
  double sigma_mu_hat() const {
    const char* poi = fPoiName.Data();
    double orig_alt_val = alt_val;
    if(fabs(w->var(poi)->getVal() - orig_alt_val) < 1e-9) {
      //shift alt_val so that it's different to testing value ...
      if(w->var(poi)->getVal() - 1. >= w->var(poi)->getMin()) orig_alt_val -= 1.;
      else orig_alt_val += 1.;
    }
  
    double sigma = w->sigma_mu(poi,TString::Format("asimovData%d",int(orig_alt_val)),orig_alt_val);
    
    return sigma;
  }
  
  std::pair<double,double> expected_null_pval(double n_sigma=0) {
    nSigma = new double(n_sigma);
    auto out = pval();
    delete nSigma;
    return out;
  }
  
  std::pair<double,double> expected_alt_pval(double n_sigma=0) {
    nSigma = new double(n_sigma);
    auto out = pval(1);
    delete nSigma;
    return out;
  }
  
  std::pair<double,double> expected_cls_pval(double n_sigma=0) {
    nSigma = new double(n_sigma);
    auto out = pval(2);
    delete nSigma;
    return out;
  }
  
  std::pair<double,double> null_pval() { return pval(0); }
  std::pair<double,double> alt_pval() { return pval(1); }
  std::pair<double,double> cls_pval() { return pval(2); }

  
  //returns the pValue and its error
  //pType=0 is null, pType=1 is alt, pType=2 is cls
  std::pair<double,double> pval(int pType=0) const {
  
      const char* poi = fPoiName.Data();
      const char* arg = (fArgName!="") ? fArgName.Data() : 0;
  
      double x = w->var(poi)->getVal();
  
      //indicate the p-value we are about to do by setting it's value to 0.5 and error to 0.5 
      
      //save the p-values 
      if(arg) {
        double x0 = w->var(arg)->getVal();
        nullPVals2.SetPoint(nullPVals2.GetN(),x0,x,0.5);
        altPVals2.SetPoint(altPVals2.GetN(),x0,x,0.5);
        clsPVals2.SetPoint(clsPVals2.GetN(),x0,x,0.5);
      } else {
        //guess p-value will be the interpolated value ...
        //find first point before x and will use that to give an error ...
        int i=0;
        for(i=0;i<nullPVals.GetN();i++) if(nullPVals.GetX()[i]>= x) {i--;break;}
      
        nullPVals.SetPoint(nullPVals.GetN(),x, (nullPVals.GetN()<2) ? 0.5 : std::max(nullPVals.Eval(x),0.));
        altPVals.SetPoint(altPVals.GetN(),x, (altPVals.GetN()<2) ? 0.5 : std::max(altPVals.Eval(x),0.));
        clsPVals.SetPoint(clsPVals.GetN(),x, (clsPVals.GetN()<2) ? 0.5 : std::max(clsPVals.Eval(x),0.));
        nullPVals.SetPointError(nullPVals.GetN()-1,0, (i==0) ? 0.5 : fabs( nullPVals.GetY()[nullPVals.GetN()-1] - nullPVals.GetY()[i] ) );
        altPVals.SetPointError(altPVals.GetN()-1,0,   (i==0) ? 0.5 : fabs( altPVals.GetY()[altPVals.GetN()-1] - altPVals.GetY()[i] ));
        clsPVals.SetPointError(clsPVals.GetN()-1,0,   (i==0) ? 0.5 : fabs( clsPVals.GetY()[clsPVals.GetN()-1] - clsPVals.GetY()[i] ));
        nullPVals.Sort(); altPVals.Sort(); clsPVals.Sort();
      }
      
      if(mainPad) { mainPad->Modified(1); mainPad->Update(); }
      

      
      
      std::pair<double,double> pVals;
      std::pair<double,double> pValErrs = std::make_pair(0.,0.);
      
      
      
      if(!gPad) gROOT->MakeDefCanvas();
      
      gPad->Clear(); //remove any previous stuff
      gPad->SetLogy();
      //some text to the pad to indicate the value we are testing 
      TLatex* t  = new TLatex; t->SetText(gPad->GetLeftMargin(),1.-gPad->GetTopMargin(),Form("Testing: %s = %g (#sigma ...)",w->var(poi)->GetTitle(),x));
      t->SetNDC(1);
      t->Draw();
      gPad->Modified(1);gPad->Update();gPad->GetCanvas()->Modified(1);gPad->GetCanvas()->Update();

      //do not reuse sigma_mu because it's a function of poi ... so must be re-evaluated at each time
      double sigma = sigma_mu_hat();
      
      if(t) {
        t->SetText( t->GetX(), t->GetY(), Form("Testing: %s = %g (#sigma = %g) (t ...)",w->var(poi)->GetTitle(),x,sigma) );
        gPad->Modified(1);gPad->Update();
      }
      
      double _pll = 0;
      
      std::vector<double> toy_plls;
      int n_nans = 0;
      
      RooAbsPdf::GenSpec* gsAlt = 0; RooAbsReal* altNll = 0; //if we don't reuse nll it seems like we leak?? FIXME
      RooAbsPdf::GenSpec* gsNull = 0; RooAbsReal* nullNll = 0; //if we don't reuse nll it seems like we leak?? FIXME
      
      if(nSigma) {
        //doing expected p-value 
        
        if(nToysMin>0) {
          
          
          
          
          //use toys under alt distribution to compute expected ts value ...
          TH1* altDist = new TH1D("alt",Form("p(t|%s=%g)",w->var(poi)->GetTitle(),alt_val),50,-1,1);  altDist->SetCanExtend(TH1::kXaxis);
          altDist->SetDirectory(0);altDist->SetLineColor(kRed);
          altDist->SetStats(false);altDist->SetBit(TH1::kNoTitle);
          altDist->Draw();
          TH1* nanAltDist = (TH1*)altDist->Clone("nanAlt"); nanAltDist->SetFillColor(kRed); nanAltDist->SetDirectory(0); nanAltDist->SetBit(kCanDelete);
          nanAltDist->Draw("hist same");
          t->Draw("same");
          
          //show the pll too ... use a TGraph so that appears inside the axis 
          TGraph* obsPll = new TGraph; obsPll->SetName("t");
          obsPll->SetLineWidth(2);obsPll->SetMarkerStyle(0);obsPll->SetLineStyle(2);

          obsPll->SetBit(kCanDelete);
          obsPll->Draw("LSAME");
      
          obsPll->SetPoint(0,0,0);obsPll->SetPoint(1,0,0);
      
          auto leg = new TLegend(1.-gPad->GetRightMargin()-0.3,1.-gPad->GetTopMargin()-0.4,1.-gPad->GetRightMargin()-0.03,1.-gPad->GetTopMargin()-0.03); leg->SetFillStyle(1001); leg->SetLineColor(0);
          leg->AddEntry(altDist,altDist->GetTitle(),"l");
          TLegendEntry* e1 =  leg->AddEntry(obsPll,TString::Format("%s^{%d#sigma}=%g","t",int(*nSigma),0.),"l");
          
          leg->SetBit(kCanDelete);
          leg->Draw();
          
          
          auto tmpLvl = gErrorIgnoreLevel;
          gErrorIgnoreLevel = kFatal;
          
          for(int i=0;i<nToysMin;i++) {
            if(i%10==0) {
              t->SetText( t->GetX(), t->GetY(), Form("Finding expected (%s = %g, test %g) (%d/%d toys ...)",w->var(poi)->GetTitle(),alt_val,x,i,nToysMin) );
              
              if(i>0) {
                float targetTailIntegral = ROOT::Math::normal_cdf(*nSigma);
                int target_n = toy_plls.size()*(1.-targetTailIntegral);
                
                std::nth_element(toy_plls.begin(),toy_plls.begin()+target_n,toy_plls.end());
                _pll = toy_plls[target_n];
                obsPll->SetPoint(0,_pll,0);obsPll->SetPoint(1,_pll,altDist->GetBinContent(altDist->GetXaxis()->FindFixBin(_pll))*10);
                e1->SetLabel(TString::Format("%s^{%d#sigma}=%g","t",int(*nSigma),_pll));
              }
              gPad->Modified(1);gPad->Update();
            }
            
            //evaluate an alt-hypothesis toy ...
            w->var(poi)->setVal(alt_val);
            auto toy = w->generateToy("myToy","My Toy",false/*fit to obs data*/,&gsAlt);
            w->var(poi)->setVal(x);
            double toy_pll = w->pll(*w->var(poi), toy.first, toy.second, fPLLTestStatisticType,&altNll);
            delete toy.first; delete toy.second;
          
            if(std::isnan(toy_pll)) {
              toy_pll = 0; //assume it was going to be about 0
              nanAltDist->Fill(toy_pll); n_nans++;
            }
          
            altDist->Fill(toy_pll);
            
            toy_plls.push_back(toy_pll);
            
            

          }
          
          gErrorIgnoreLevel = tmpLvl;
          
          float targetTailIntegral = ROOT::Math::normal_cdf(*nSigma);
          int target_n = toy_plls.size()*(1.-targetTailIntegral);
          
          std::nth_element(toy_plls.begin(),toy_plls.begin()+target_n,toy_plls.end());
          _pll = toy_plls[target_n];
          
          
          
        } else {
        
          //find the solution (wrt x) of: TRooFit::Asymptotics::altPValue(x, var(poi), alt_val, _sigma_mu, fPLLTestStatisticType) - targetPValue = 0 
          float targetTailIntegral = ROOT::Math::normal_cdf(*nSigma);
          
          TailIntegralFunction f( w->var(poi), alt_val, sigma, fPLLTestStatisticType  , targetTailIntegral );
          ROOT::Math::BrentRootFinder brf;
          ROOT::Math::WrappedFunction<TailIntegralFunction> wf(f);
          
          double _tpll(500.);
          double currVal(1.);
          int tryCount(0);
          while( fabs( TRooFit::Asymptotics::altPValue(_tpll, w->var(poi), alt_val, sigma, fPLLTestStatisticType) - targetTailIntegral ) > 1e-4 ) {
            currVal = TRooFit::Asymptotics::altPValue(_tpll, w->var(poi), alt_val, sigma, fPLLTestStatisticType);
            if(currVal - targetTailIntegral > 1e-4) _tpll = 2.*(_tpll+1.);
            else if(currVal - targetTailIntegral < -1e-4) _tpll /= 2.;
            
            brf.SetFunction( wf, 0, _tpll);
            brf.Solve();
            _tpll =  brf.Root();
            //std::cout << " -- " << brf.Root() << " " << TRooFit::Asymptotics::altPValue(_tpll, w->var(poi), alt_val, sigma, fPLLTestStatisticType) << std::endl;
            tryCount++;
            if(tryCount>20) {
              break;
            }
        
          }
          _pll = _tpll*0.99; //subtract a little to capture delta function effects
      
        } 
     
      } else {
        //doing observed pValue
        _pll = w->pll(*w->var(poi),w->data(),w->data_gobs(),fPLLTestStatisticType,0,&unconditionalFit);
        
      }
      
      //pVals  = (nSigma) ? :pVals = w->asymptoticExpectedPValue(poi,fPLLTestStatisticType,*nSigma)
      //                           w->asymptoticPValue(poi,w->data(),w->data_gobs(),fPLLTestStatisticType,0,0,&unconditionalFit);
      
      
      //range of tsDist will be 2*_pll so that _pll is at the center!
      TH1* nullDist = new TH1D("null",Form("p(t|%s=%g)",w->var(poi)->GetTitle(),x),50,std::min(2*_pll,-1.),std::max(2*_pll,1.)); 
      
      if(fPLLTestStatisticType==TRooFit::OneSidedPositive) nullDist->GetXaxis()->SetTitle("Upper Limit Test Statistic");
      else if(fPLLTestStatisticType==TRooFit::OneSidedNegative) nullDist->GetXaxis()->SetTitle("Capped Discovery Test Statistic");
      else if(fPLLTestStatisticType==TRooFit::Uncapped) nullDist->GetXaxis()->SetTitle("Uncapped Discovery Test Statistic");
      else nullDist->GetXaxis()->SetTitle("Custom Test Statistic");
      

      nullDist->SetDirectory(0);nullDist->SetLineColor(kBlue);
      TH1* altDist = new TH1D("alt",Form("p(t|%s=%g)",w->var(poi)->GetTitle(),alt_val),50,std::min(2*_pll,-1.),std::max(2*_pll,1.)); 
      altDist->SetDirectory(0);altDist->SetLineColor(kRed);
      nullDist->SetBit(kCanDelete);altDist->SetBit(kCanDelete);
      TH1* nullDistShaded = (TH1*)nullDist->Clone("nullShaded"); nullDistShaded->SetFillColor(kBlue); nullDistShaded->SetFillStyle(3004); nullDistShaded->SetLineWidth(0);
      TH1* altDistShaded = (TH1*)altDist->Clone("altShaded"); altDistShaded->SetFillColor(kRed); altDistShaded->SetFillStyle(3005); altDistShaded->SetLineWidth(0);
      nullDistShaded->SetDirectory(0);altDistShaded->SetDirectory(0);
      
      nullDist->SetStats(false);nullDist->SetBit(TH1::kNoTitle);
      nullDist->Draw();altDist->Draw("same");nullDistShaded->Draw("same");altDistShaded->Draw("same");
      t->SetBit(kCanDelete);t->Draw();
      //show the pll too ... use a TGraph so that appears inside the axis 
      TGraph* obsPll = new TGraph; obsPll->SetName("t");
      obsPll->SetLineWidth(2);obsPll->SetMarkerStyle(0);
      
      if(nSigma) {
        obsPll->SetLineStyle(2);
      }
      
      obsPll->SetBit(kCanDelete);
      obsPll->Draw("LSAME");
      
      obsPll->SetPoint(0,_pll,0);obsPll->SetPoint(1,_pll,std::max(nullDist->GetBinContent(nullDist->GetXaxis()->FindFixBin(_pll)),
                                                                                                altDist->GetBinContent(altDist->GetXaxis()->FindFixBin(_pll)))*10);
      
      auto leg = new TLegend(1.-gPad->GetRightMargin()-0.3,1.-gPad->GetTopMargin()-0.4,1.-gPad->GetRightMargin()-0.03,1.-gPad->GetTopMargin()-0.03); leg->SetFillStyle(1001); leg->SetLineColor(0);
      leg->AddEntry(nullDist,nullDist->GetTitle(),"l");
      leg->AddEntry(altDist,altDist->GetTitle(),"l");
      if(nSigma) {
         leg->AddEntry(obsPll,TString::Format("%s^{%d#sigma}=%g","t",int(*nSigma),_pll),"l");
      } else {
        leg->AddEntry(obsPll,TString::Format("%s^{obs}=%g","t",_pll),"l");
      }
      
      //add the p-values to the legend too
      TLegendEntry* e1 = leg->AddEntry(nullDistShaded,nullDistShaded->GetTitle(),"f");
      TLegendEntry* e2 = leg->AddEntry(altDistShaded,altDistShaded->GetTitle(),"f");
      leg->SetBit(kCanDelete);
      leg->Draw();
          
          
          
      if(nToysMin>0) {
        //generate nMinToys to calculate pValue ...
        
        //will display the asymptotic curves for comparison ...
        TH1* asympNullDist = (TH1*)nullDist->Clone("asympNull"); asympNullDist->SetLineStyle(2); asympNullDist->SetDirectory(0); asympNullDist->SetBit(kCanDelete); asympNullDist->Sumw2();
        TH1* asympAltDist = (TH1*)altDist->Clone("asympAlt"); asympAltDist->SetLineStyle(2); asympAltDist->SetDirectory(0); asympAltDist->SetBit(kCanDelete);asympAltDist->Sumw2();
        asympNullDist->Draw("l same");
        asympAltDist->Draw("l same");
        
        TH1* nanNullDist = (TH1*)nullDist->Clone("nanNull"); nanNullDist->SetFillColor(kBlue); nanNullDist->SetDirectory(0); nanNullDist->SetBit(kCanDelete); nanNullDist->Sumw2();
        TH1* nanAltDist = (TH1*)altDist->Clone("nanAlt"); nanAltDist->SetFillColor(kRed); nanAltDist->SetDirectory(0); nanAltDist->SetBit(kCanDelete);nanAltDist->Sumw2();
        nanNullDist->Draw("hist same");
        nanAltDist->Draw("hist same");
        
        //reuse the toy_plls we already generated, if available
        for(auto toy_pll : toy_plls) {
          if(toy_pll==0 && n_nans>0) { toy_pll = std::numeric_limits<double>::quiet_NaN(); n_nans--; }
          if(std::isnan(toy_pll)) {
            //assume toy_pll would have been close to 0 ... so put it at 0 ...
            toy_pll = 0;
            nanAltDist->Fill(toy_pll);
          } 
          altDist->Fill(toy_pll); if(toy_pll >= _pll) altDistShaded->Fill(toy_pll);
        }
        
        for(int i=1;i<=nullDist->GetNbinsX();i++) {
            asympNullDist->SetBinContent(i, (TRooFit::Asymptotics::nullPValue( nullDist->GetXaxis()->GetBinLowEdge(i), w->var(poi), sigma, fPLLTestStatisticType) - 
                                       TRooFit::Asymptotics::nullPValue( nullDist->GetXaxis()->GetBinUpEdge(i), w->var(poi), sigma, fPLLTestStatisticType)) );
            asympAltDist->SetBinContent(i, (TRooFit::Asymptotics::altPValue( altDist->GetXaxis()->GetBinLowEdge(i), w->var(poi), alt_val, sigma, fPLLTestStatisticType) - 
                                       TRooFit::Asymptotics::altPValue( altDist->GetXaxis()->GetBinUpEdge(i), w->var(poi), alt_val, sigma, fPLLTestStatisticType)));
        }
        
        //while generating toys, suppress errors that would come from pll returning nan (because conditional nll < unconditional nll)
        auto tmpLvl = gErrorIgnoreLevel;
        gErrorIgnoreLevel = kFatal;
        
        int numToys = nToysMin;
        
        
        for(int j=0;j<numToys;j++) {
          if(j%10==0) {
            t->SetText( t->GetX(), t->GetY(), Form("Testing: %s = %g (#sigma = %g) (%d/%d toys ...)",w->var(poi)->GetTitle(),x,sigma,j,numToys) );
            gPad->Modified(1);gPad->Update();
          }
          
          //auto toy = TRooFit::generateToy( w->model(), w->set("obs"), w->gobs() /*,true -- binned*/ ); //FIXME - save the genspec!
          auto toy =  w->generateToy("myToy","My Toy",false/*fit to obs data*/,&gsNull);
          
          //evaluate ts value for the toy, fill this into nullDist and (if bigger than _pll) nullDistShaded
          double toy_pll = w->pll(*w->var(poi), toy.first, toy.second, fPLLTestStatisticType,&nullNll);
          delete toy.first; delete toy.second;
          
          if(std::isnan(toy_pll)) {
            //assume toy_pll would have been close to 0 ... so put it at 0 ...
            toy_pll = 0;
            nanNullDist->Fill(toy_pll);
          } 
          nullDist->Fill(toy_pll); if(toy_pll >= _pll) nullDistShaded->Fill(toy_pll);
          
          
          //evaluate an alt-hypothesis toy ... if we already have toy_plls (happens when doing expected limits) then reuse those ..
          if(toy_plls.size()==0) {
            w->var(poi)->setVal(alt_val);
            toy =  w->generateToy("myToy","My Toy",false/*fit to obs data*/,&gsAlt);
            w->var(poi)->setVal(x);
            toy_pll = w->pll(*w->var(poi), toy.first, toy.second, fPLLTestStatisticType,&altNll);
            delete toy.first; delete toy.second;
            if(std::isnan(toy_pll)) {
              //assume toy_pll would have been close to 0 ... so put it at 0 ...
              toy_pll = 0;
              nanAltDist->Fill(toy_pll);
            } 
            altDist->Fill(toy_pll); if(toy_pll >= _pll) altDistShaded->Fill(toy_pll);
          }
          
          if(j+1 == numToys) {
            //about to finish ... check we have at least one toy in nullDistShaded
            if(nullDistShaded->GetEntries()==0) {
              int guessEvents = 1.1/TRooFit::Asymptotics::nullPValue(_pll, w->var(poi), sigma, fPLLTestStatisticType);
              gErrorIgnoreLevel = tmpLvl;
              Warning("pval","More toys needed to get a non-zero p-value for null .. guessing require %d from asymptotics",guessEvents);
              if(guessEvents > nToysMax) {
                 Error("pval","Guess required toys greater than maximum (%d) ... will not generate any more toys",nToysMax);
              } else {
                numToys = guessEvents;
              }
              gErrorIgnoreLevel = kFatal;
            }
          } else if(numToys > nToysMin && nullDistShaded->GetEntries()>0) {
            //generated a toy for the null p-value ... so exit the loop
            break;
          }
          
          
          if(j%10==0) {
            //update display ...
            nullDistShaded->SetTitle(Form("%.3f",double(nullDistShaded->GetEntries())/nullDist->GetEntries())); e1->SetLabel(nullDistShaded->GetTitle());
            altDistShaded->SetTitle(Form("%.3f",double(altDistShaded->GetEntries())/altDist->GetEntries())); e2->SetLabel(altDistShaded->GetTitle());
            obsPll->SetPoint(0,_pll,0);obsPll->SetPoint(1,_pll,std::max(nullDist->GetBinContent(nullDist->GetXaxis()->FindFixBin(_pll)),
                                                                                                altDist->GetBinContent(altDist->GetXaxis()->FindFixBin(_pll)))*10);
            asympNullDist->Scale( nullDist->GetEntries() );
            asympAltDist->Scale( altDist->GetEntries() );
            gPad->Modified(1);gPad->Update();
            asympNullDist->Scale( 1./nullDist->GetEntries() );
            asympAltDist->Scale( 1./altDist->GetEntries() );
            
          }
          
          
        }
        
        gErrorIgnoreLevel = tmpLvl;
        
        if(gsNull) delete gsNull;
        if(gsAlt) delete gsAlt;
        if(nullNll) delete nullNll;
        if(altNll) delete altNll;
        
        
        pVals.first = double(nullDistShaded->GetEntries())/nullDist->GetEntries();
        pVals.second = double(altDistShaded->GetEntries())/altDist->GetEntries();
        asympNullDist->Scale( nullDist->GetEntries() );
        asympAltDist->Scale( altDist->GetEntries() );
        
        pValErrs.first = fabs(TEfficiency::Bayesian(nullDist->GetEntries(),nullDistShaded->GetEntries(),0.68,1,1,(pVals.first<0.5)/*says if getting upper limit or lower limit*/) - pVals.first);
        pValErrs.second = fabs(TEfficiency::Bayesian(altDist->GetEntries(),altDistShaded->GetEntries(),0.68,1,1,(pVals.second<0.5)/*says if getting upper limit or lower limit*/) - pVals.second);
        
      
      } else {
          //use asymptotic formulae for pvalues
          if(std::isnan(_pll)) {
            pVals = std::make_pair( std::numeric_limits<double>::quiet_NaN() , std::numeric_limits<double>::quiet_NaN() );
          } else {
            //if(fabs(_pll) < 1e-9) pVals = std::make_pair(1,1);  do we need this??
            pVals = std::make_pair( TRooFit::Asymptotics::nullPValue(_pll, w->var(poi), sigma, fPLLTestStatisticType) , 
                          TRooFit::Asymptotics::altPValue(_pll, w->var(poi), alt_val, sigma, fPLLTestStatisticType) );
          }
          
          for(int i=1;i<=nullDist->GetNbinsX();i++) {
            nullDist->SetBinContent(i, (TRooFit::Asymptotics::nullPValue( nullDist->GetXaxis()->GetBinLowEdge(i), w->var(poi), sigma, fPLLTestStatisticType) - 
                                       TRooFit::Asymptotics::nullPValue( nullDist->GetXaxis()->GetBinUpEdge(i), w->var(poi), sigma, fPLLTestStatisticType)) / nullDist->GetXaxis()->GetBinWidth(i) );
            altDist->SetBinContent(i, (TRooFit::Asymptotics::altPValue( altDist->GetXaxis()->GetBinLowEdge(i), w->var(poi), alt_val, sigma, fPLLTestStatisticType) - 
                                       TRooFit::Asymptotics::altPValue( altDist->GetXaxis()->GetBinUpEdge(i), w->var(poi), alt_val, sigma, fPLLTestStatisticType)) / altDist->GetXaxis()->GetBinWidth(i) );
            
            if(nullDist->GetXaxis()->GetBinLowEdge(i) >= _pll) nullDistShaded->SetBinContent(i,nullDist->GetBinContent(i));
            if(altDist->GetXaxis()->GetBinLowEdge(i) >= _pll) altDistShaded->SetBinContent(i,altDist->GetBinContent(i));
            
          }
          nullDistShaded->SetTitle(Form("%g",pVals.first)); e1->SetLabel(nullDistShaded->GetTitle());
          altDistShaded->SetTitle(Form("%g",pVals.second)); e2->SetLabel(altDistShaded->GetTitle());
       }
       
       t->SetText( t->GetX(), t->GetY(), Form("Testing: %s = %g (#sigma = %g)",w->var(poi)->GetTitle(),x,sigma) );
       
       obsPll->SetPoint(0,_pll,0);obsPll->SetPoint(1,_pll,std::max(nullDist->GetBinContent(nullDist->GetXaxis()->FindFixBin(_pll)),
                                                                   altDist->GetBinContent(altDist->GetXaxis()->FindFixBin(_pll)))*10);
          
       gPad->Modified(1);gPad->Update();
        
      
      
      if(std::isnan(pVals.first) || std::isnan(pVals.second)) {
        std::cout << "ERROR: NaN PValue for " << poi << " = " << x << std::endl;
        throw x;
      }
      
      std::pair<double,double> out = std::make_pair(pVals.first,pValErrs.first);
      
      if(pType==1) out = std::make_pair(pVals.second,pValErrs.second);
      
      double clsVal = (!pVals.second) ? 1 : pVals.first / pVals.second;
      
      double clsErr = (clsVal)*sqrt( pow(pValErrs.first/pVals.first,2) + pow(pValErrs.second/pVals.second,2) );
      if(pVals.first==0 && pVals.second) clsErr = pValErrs.second/pVals.second;
      
      if(pType==2) { out.first = clsVal; out.second = clsErr; }
      
      //save the p-values 
      if(arg) {
        double x0 = w->var(arg)->getVal();
        nullPVals2.SetPoint(nullPVals2.GetN()-1,x0,x,pVals.first);
        altPVals2.SetPoint(altPVals2.GetN()-1,x0,x,pVals.second);
        clsPVals2.SetPoint(clsPVals2.GetN()-1,x0,x,pVals.first / pVals.second);
      } else {
        //because we sorted earlier, we have to find the correct point again
        int i(0);
        for(i=0;i<nullPVals.GetN();i++) if( fabs(nullPVals.GetX()[i]-x)<1e-12 ) break;
      
        nullPVals.SetPoint(i,x,pVals.first);
        altPVals.SetPoint(i,x,pVals.second);
        clsPVals.SetPoint(i,x, clsVal);
        nullPVals.SetPointError(i,0,pValErrs.first);
        altPVals.SetPointError(i,0,pValErrs.second);
        clsPVals.SetPointError(i,0,clsErr);
        
        nullPVals.Sort(); altPVals.Sort(); clsPVals.Sort();
      }
      
      if(mainPad) { mainPad->Modified(1); mainPad->Update(); }

      //prepar the return result ... a pair consisting of the required pValue and its error
      return out;
   }
   
   
   //this method is used by BrentRootFinder
   double operator() (double x) const {
      //cycle through subpads of the parent pad ...
      if(!gPad) gPad = gROOT->MakeDefCanvas();
      auto currPad = gPad;
      if( gPad->GetMother()->cd( currPad->GetNumber()+1 ) == currPad || gPad->GetMother()->cd( currPad->GetNumber()+1 ) == nullptr ) {
        currPad->GetMother()->cd(1); //cycle back to first pad
      }
      w->var(fPoiName)->setVal(x);
      return pval(CLs?2:0).first - alpha;
   }
    
      TRooWorkspace* w;

    bool CLs = true;

    double alpha = 0.05;
    double* nSigma = 0;
    mutable double* sigma_mu = 0;
    const char* arg = 0;
    mutable RooFitResult* unconditionalFit = 0;
    
    mutable TGraphErrors nullPVals;
    mutable TGraphErrors altPVals;
    mutable TGraphErrors clsPVals;
    
    mutable TGraph2DErrors nullPVals2;
    mutable TGraph2DErrors altPVals2;
    mutable TGraph2DErrors clsPVals2;
    
    mutable TVirtualPad* mainPad = 0;

    double alt_val = 0; //alt value of the poi
    
    int nToysMin = 0;
    int nToysMax = 50000;
    
    
     bool kCLs = true; //use cls-pvalue when finding limit
    bool kAsymptotics = true;
    
    double fAltValue = 0; //alternative hypothesis poi value
    
    
    TString fObsData = ""; //set to the observed data ... will run fits to this data before generating toys
    
    
    
    TString fPoiName = "mu_SIG"; //name of the parameter of interest
    TString fArgName = ""; //name of a argument (const parameter) that we can scan over
    bool (*fPLLTestStatisticType)(double mu, double mu_hat) = TRooFit::OneSidedPositive; //OneSidedPositive is the recommended for upper limits
    
    
    
    
    //finds asymtptotic limit using brent root finder with 3 initial points
double FindLimit() {

  const char* poi = fPoiName.Data();

  //check workspace for any var containing a LimitScan binning that isn't the poi ...
  RooArgSet allVars = w->allVars();
  RooFIter itr = allVars.fwdIterator();
  RooAbsArg* a = 0;
  RooRealVar* arg = 0;
  while( (a = itr.next()) ) {
    if(strcmp(a->GetName(),poi)==0) continue;
    if(!a->hasRange("LimitScan")) continue;
    if(!a->getAttribute("Constant")) continue; //only look at const parameters
    if(arg) {
      std::cout << "ERROR: Only one argument can have a LimitScan binning" << std::endl;
      return 0;
    }
    arg = dynamic_cast<RooRealVar*>(a);
  }
  
  if(arg) fArgName = arg->GetName();

  //PValueFunction pvals(w,poi,CLs,fPLLTestStatisticType,alpha,0,(arg)?arg->GetName():0);
  if(!kAsymptotics) nToysMin=2000;
  
  TVirtualPad* pad = gPad;
  if(!pad) {
    gROOT->MakeDefCanvas();
    pad = gPad;
  }
  
  gPad->GetCanvas()->cd();
  gPad->Clear(); //clears the existing canvas
  
  mainPad = new TPad("pvals","pvals",0,0.3,1,1);
  mainPad->Draw();
  
  auto tsDists = new TPad("tsDists","tsDists",0,0,1,0.3);
  tsDists->Divide(4,1);
  tsDists->Draw();
  
  
  TMultiGraph gg;
  auto leg = new TLegend(1.-gPad->GetRightMargin()-0.3,1.-gPad->GetTopMargin()-0.4,1.-gPad->GetRightMargin()-0.03,1.-gPad->GetTopMargin()-0.03); leg->SetFillStyle(1001); leg->SetLineColor(0);
  if(!arg) {
    //draw a target line ....
    TGraph* g_alpha = new TGraph; 
    g_alpha->SetPoint( 0, w->var(poi)->getMin("LimitScan"), alpha );
    g_alpha->SetPoint( 1, w->var(poi)->getMax("LimitScan"), alpha );
    g_alpha->SetLineStyle(2);
    g_alpha->SetBit(kCanDelete);
    gg.Add(g_alpha);
  
    gg.Add(&nullPVals); gg.Add(&altPVals); gg.Add(&clsPVals);
    mainPad->cd();
    gg.Draw("ALP");
    leg->AddEntry(&nullPVals,"Null p-value","l");
    leg->AddEntry(&altPVals,"Alt. p-value","l");
    leg->AddEntry(&clsPVals,"CLs p-value","l");
    leg->Draw();
  }
  
  
  ROOT::Math::WrappedFunction<TRooHypoTester&> wf(*this);
  
  double out = 0;
  
  int currBin = (arg) ? arg->getBin("LimitScan") : 0;
  int numBins = (arg) ? arg->numBins("LimitScan") : 1;

  int exceptionCount = 0;

  if(arg) { 
    TRooFit::msg().Info("FindLimit","Scanning %s at %d points",arg->GetName(),numBins);
  }

  tsDists->cd(4); //switch to last subpad before starting loop (first action will be to move to first subpad)
  for(int i=0;i<numBins;i++) {
    if(arg) arg->setBin(i,"LimitScan");
    
    ROOT::Math::BrentRootFinder brf;
    brf.SetNpx(3);
    brf.SetFunction( wf, w->var(poi)->getMin("LimitScan"), w->var(poi)->getMax("LimitScan") );
    reset();
  
    try {
      brf.Solve(100,0,1e-2);
    } catch(double badX) {
      reset();

      if( fabs(w->var(poi)->getMin("LimitScan") - badX) < fabs(w->var(poi)->getMax("LimitScan") - badX) && badX+(w->var(poi)->getMax("LimitScan")-badX)*0.05  <= 0 ) {
        w->var(poi)->setMin("LimitScan",badX+(w->var(poi)->getMax("LimitScan")-badX)*0.05 );
      } else {
        w->var(poi)->setMax("LimitScan",badX+(w->var(poi)->getMin("LimitScan")-badX)*0.05 );
      }
      w->loadFit(0); //reset to nominal values
      TRooFit::msg().Warning("FindLimit","Bad p-values detected @ %g ... restricting LimitScan range to [%g,%g]",badX, w->var(poi)->getMin("LimitScan"), w->var(poi)->getMax("LimitScan") );
      i--;
      exceptionCount++;
      if(exceptionCount==3) {
        TRooFit::msg().Error("FindLimit","Too many bad fits, please try using a more sensible LimitScan range");
        throw badX; //stop after moving the range too many times
      }
      continue;
    }
    
    if(i==currBin) out = brf.Root();
    if(arg) {
      TRooFit::msg().Info("FindLimit","%s=%g limit: %g",arg->GetName(),arg->getVal(),brf.Root());
    }
  }

  if(arg) arg->setBin(currBin,"LimitScan");
  
  //before ending, draw a copy of the main graph
  mainPad->cd();
  auto ggClone = gg.Clone();
  ggClone->SetBit(kCanDelete);
  ggClone->Draw("ALP");
  leg->SetBit(kCanDelete);
  leg->Draw();
  
  reset();

  return out;
}
    
    
    //void SetScanRange(double low, double high) { poi()->setRange("LimitScan",low,high); }
    
    //value to test
    //void SetValuesToTest(double nullPoiVal, double altPoiVal) { poi()->setVal(val); fAltValue = altPoiVal; }
    
    
    
   
    
    
   
    
    std::vector<TRooFitResult*> fFitResults; //saved fit results
    
    
    /*
    
    struct HypoPoint {
      TRooWorkspace* w;
      TString poiName;
      double nullPoiValue; //value being tested
      double altPoiValue; //alt hypothesis
      bool (*compatFunction)(double mu, double mu_hat); //compatibility function for ts definition
      
      double* obs_ts_val = 0; //the observed data test statistic value
      
      double* sigma_mu_hat = 0; //the estimate of mu_hat's standard deviation
    
      TH1* fNullDist;
      TH1* fAltDist;
      
      double sigmaMuHat() {
        double sigma = 0;

        if(sigma_mu_hat) sigma = *sigma_mu_hat;
        if(sigma<=0) {
          sigma = w->sigma_mu(poi,TString::Format("asimovData%g",altPoiValue),altPoiValue);
          if(!sigma_mu_hat) sigma_mu_hat = new double(sigma); //saves value
        }
      }
      
      double obsTestStatVal() {
        if(!obs_ts_val) {
          w->var(poiName)->setVal(nullPoiValue);
          obs_ts_val = new double( w->pll( *w->var(poiName), w->data(obsDataName), w->data_gobs(obsDataName), compatFunction ) );
        }
        return *obs_ts_val;
      }
      
      
      
      
    
    };
    
    HypoPoint* fCurrentHypoPoint;
    */
  
  
};

#endif