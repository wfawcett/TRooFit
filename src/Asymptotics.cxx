

#include "TRooFit/Utils.h"
#include "Math/ProbFuncMathCore.h"

TRooFit::Asymptotics::Asymptotics() : TObject() { }

Double_t TRooFit::Asymptotics::Phi_m(double mu, double mu_prime, double a, double sigma, bool (*compatFunc)(double mu, double mu_hat) ) {
   //implemented only for special cases of compatibility function
   if(compatFunc==TwoSided) {
      return 0;
   } 

   if(compatFunc==OneSidedPositive) {
      //ignore region below x*sigma+mu_prime = mu ... x = (mu - mu_prime)/sigma 
      if(a < (mu-mu_prime)/sigma) return 0;
      return ROOT::Math::gaussian_cdf(a) - ROOT::Math::gaussian_cdf((mu-mu_prime)/sigma);
   } else if(compatFunc==OneSidedNegative) {
      //ignore region above x*sigma+mu_prime = mu ... 
      if(a > (mu-mu_prime)/sigma) return ROOT::Math::gaussian_cdf((mu-mu_prime)/sigma);
      return ROOT::Math::gaussian_cdf(a);
   } else if(compatFunc==OneSidedAbsolute) {
      //cutoff at x*sigma+mu_prime = mu for mu<0 , and turns back on at x*sigma+mu_prime=mu for mu>0
      //ignore region between x = (-mu-mu_prime)/sigma and x = (mu-mu_prime)/sigma
      double edge1 = (-mu - mu_prime)/sigma;
      double edge2 = (mu-mu_prime)/sigma;

      if(edge1>edge2) { double tmp=edge1; edge1=edge2; edge2=tmp; }

      if(a<edge1) return ROOT::Math::gaussian_cdf(a);
      if(a<edge2) return ROOT::Math::gaussian_cdf(edge1);
      return ROOT::Math::gaussian_cdf(a) - (ROOT::Math::gaussian_cdf(edge2) - ROOT::Math::gaussian_cdf(edge1));
   } 
   msg().Error("Phi_m","Unknown compatibility function ... can't evaluate");
   return 0;
}


//must use asimov dataset corresponding to mu_prime, evaluating pll at mu
Double_t TRooFit::Asymptotics::PValue(double k, double poiVal, double poi_primeVal, double sigma, double lowBound, double upBound, bool (*compatFunc)(double mu, double mu_hat)) {
  //uncapped test statistic is equal to onesidednegative when k is positive, and equal to 1.0 - difference between twosided and onesidednegative when k is negative ...
  if(compatFunc==Uncapped) {
    //if(k==0) return 0.5;
    if(k>0) return PValue(k,poiVal,poi_primeVal,sigma,lowBound,upBound,OneSidedNegative);
    return 1. - (PValue(-k,poiVal,poi_primeVal,sigma,lowBound,upBound,TwoSided) - PValue(-k,poiVal,poi_primeVal,sigma,lowBound,upBound,OneSidedNegative));
  }


  //if(k<0) return 1.;
  if(k<=0) {
    if(compatFunc==OneSidedNegative && fabs(poiVal-poi_primeVal)<1e-9) return 0.5; //when doing discovery (one-sided negative) use a 0.5 pValue
    return 1.; //case to catch the delta function that ends up at exactly 0 for the one-sided tests 
  }
  
  if(sigma==0 && (lowBound!=-std::numeric_limits<double>::infinity() || upBound != std::numeric_limits<double>::infinity())) {
      msg().Error("TRooFit::Asymptotic::PValue","You must specify a sigma_mu parameter when parameter of interest is bounded");
      return -1;
   }
  
      //get the poi value that defines the test statistic, and the poi_prime hypothesis we are testing 
   //when setting limits, these are often the same value 



   Double_t Lambda_y = 0;
   if(fabs(poiVal-poi_primeVal)>1e-9) Lambda_y = (poiVal-poi_primeVal)/sigma;

   

   Double_t k_low = (lowBound == -std::numeric_limits<double>::infinity()) ? std::numeric_limits<double>::infinity() : pow((poiVal - lowBound)/sigma,2);
   Double_t k_high = (upBound == std::numeric_limits<double>::infinity()) ? std::numeric_limits<double>::infinity() : pow((upBound - poiVal)/sigma,2);

   //std::cout << " sigma = " << sigma << std::endl;

   double out = Phi_m(poiVal,poi_primeVal,std::numeric_limits<double>::infinity(),sigma,compatFunc) - 1;

   //if(fOutputLevel<=0) msg().Info("PValue","sigma=%f k_low=%f k_high=%f ", sigma, k_low, k_high);
     
   //std::cout << " out = " << out << std::endl;
   //std::cout << " k_low = " << k_low << std::endl;

   //go through the 4 'regions' ... only two of which will apply
   if( k <= k_low ) {
      out += ROOT::Math::gaussian_cdf(sqrt(k)-Lambda_y) + Phi_m(poiVal,poi_primeVal,Lambda_y - sqrt(k),sigma,compatFunc);
   } else {
      double Lambda_low = (poiVal - lowBound)*(poiVal + lowBound - 2.*poi_primeVal)/(sigma*sigma);
      double sigma_low = 2.*(poiVal - lowBound)/sigma;
      out +=  ROOT::Math::gaussian_cdf((k-Lambda_low)/sigma_low) + Phi_m(poiVal,poi_primeVal,(Lambda_low - k)/sigma_low,sigma,compatFunc);
   }
   //std::cout << " out = " << out << std::endl;
   //std::cout << " k_high = " << k_high << std::endl;

   if( k <= k_high ) {
      out += ROOT::Math::gaussian_cdf(sqrt(k)+Lambda_y) - Phi_m(poiVal,poi_primeVal,Lambda_y + sqrt(k),sigma,compatFunc);
   } else {
      double Lambda_high = (poiVal - upBound)*(poiVal + upBound - 2.*poi_primeVal)/(sigma*sigma);
      double sigma_high = 2.*(upBound-poiVal)/sigma;
      out +=  ROOT::Math::gaussian_cdf((k-Lambda_high)/sigma_high) - Phi_m(poiVal,poi_primeVal,(k - Lambda_high)/sigma_high,sigma,compatFunc);
   }


   return 1. - out;
}

#include "TRooFit/TRooWorkspace.h"
#include "Math/BrentRootFinder.h"
#include "Math/WrappedFunction.h"
#include "TGraph.h"

struct PValueFunction {
  PValueFunction(TRooWorkspace* _w, const char* _poi, bool _CLs, bool (*_compatibilityFunction)(double mu, double mu_hat), double _alpha, double* _nSigma, const char* _arg=0) :
    w(_w),poi(_poi),CLs(_CLs),compatibilityFunction(_compatibilityFunction),alpha(_alpha),nSigma(_nSigma),sigma_mu(0),arg(_arg),unconditionalFit(0)
   
  {
    nullPVals.SetMarkerStyle(20);altPVals.SetMarkerStyle(20);clsPVals.SetMarkerStyle(20);

  }
  
  ~PValueFunction() { if( sigma_mu ) delete sigma_mu; if(unconditionalFit) delete unconditionalFit; }
  
  void reset() { 
    if(sigma_mu) delete sigma_mu; 
    if(unconditionalFit) delete unconditionalFit;
    sigma_mu=0;unconditionalFit = 0;
  }
  
  double operator() (double x) const {
      //std::cout << "Scan " << x << " " << nullPVals.GetN() << std::endl;

      w->var(poi)->setVal(x);
      
      std::pair<double,double> pVals;
      //do not reuse sigma_mu because it's a function of poi ... so must be re-evaluated at each time
        pVals  = (nSigma) ? w->asymptoticExpectedPValue(poi,compatibilityFunction,*nSigma) :
                                 w->asymptoticPValue(poi,w->data(),w->data_gobs(),compatibilityFunction,0,0,&unconditionalFit);
      
      if(std::isnan(pVals.first) || std::isnan(pVals.second)) {
        std::cout << "ERROR: NaN PValue for " << poi << " = " << x << std::endl;
        throw x;
      }
      
      //save the p-values 
      if(arg) {
        double x0 = w->var(arg)->getVal();
        nullPVals2.SetPoint(nullPVals2.GetN(),x0,x,pVals.first);
        altPVals2.SetPoint(altPVals2.GetN(),x0,x,pVals.second);
        clsPVals2.SetPoint(clsPVals2.GetN(),x0,x,pVals.first / pVals.second);
      } else {
        nullPVals.SetPoint(nullPVals.GetN(),x,pVals.first);
        altPVals.SetPoint(altPVals.GetN(),x,pVals.second);
        clsPVals.SetPoint(clsPVals.GetN(),x,pVals.first / pVals.second);
        nullPVals.Sort(); altPVals.Sort(); clsPVals.Sort();
      }

      
      if(CLs) {
        if(!pVals.first && !pVals.second) return 1;
        return (pVals.first / pVals.second) - alpha;
      }
      return pVals.first - alpha;
  }
  
  TRooWorkspace* w;
  const char* poi;
  bool CLs;
  bool (*compatibilityFunction)(double mu, double mu_hat);
  double alpha;
  double* nSigma;
  mutable double* sigma_mu;
  const char* arg;
  mutable RooFitResult* unconditionalFit;
  
  mutable TGraph nullPVals;
  mutable TGraph altPVals;
  mutable TGraph clsPVals;
  
  mutable TGraph2D nullPVals2;
  mutable TGraph2D altPVals2;
  mutable TGraph2D clsPVals2;
  
};

double customRootFinder(PValueFunction& func, double min, double max, int max_eval = 50, double
rel_tolerance = 0.01) {

    // trying to find 0, for a function that is usually > 0 near min, and < 0 near max

    // do by zooming min and max in until difference is less than X% of the full range
    double stop_diff = (max-min)*rel_tolerance;

    double max_val = func(max);
    double min_val = func(min);

    if (max_val*min_val > 0) {
        std::cout << "BAD STRADDLE: " << min_val << " - " << max_val << std::endl;
        /*if (max_val > 0) {
            return customRootFinder(func,min,max*2,max_eval,rel_tolerance);
        }*/
        throw double(0);
    }

    int step_count = 2;
    while( (max - min) > stop_diff && step_count < max_eval ) {
        // evaluate the midpoint:
        double next = (max + min)/2.;
        double next_val = func( next );
        if (next_val == 0) return next;
        if (next_val*min_val > 0) {
            // on the same side of min_val;
            min = next;
            min_val = next_val;
        } else {
            max = next;
            max_val = next_val;
        }
        step_count++;
    }

    // interpolate between max and min to infer limit
    double y_delta = min_val - max_val;
    return min + (max - min)*(min_val)/y_delta;
    //return (max + min)/2.;
}




//finds asymtptotic limit using brent root finder with 3 initial points
double TRooFit::Asymptotics::FindLimit(TRooWorkspace* w, const char* poi, bool CLs, bool
(*_compatibilityFunction)(double mu, double mu_hat), double alpha, double* sigma) {

  //check workspace for any var containing a LimitScan binning that isn't the poi ...
  RooArgSet allVars = w->allVars();
  RooFIter itr = allVars.fwdIterator();
  RooAbsArg* a = 0;
  RooRealVar* arg = 0;
  while( (a = itr.next()) ) {
    if(strcmp(a->GetName(),poi)==0) continue;
    if(!a->hasRange("LimitScan")) continue;
    if(!a->getAttribute("Constant")) continue; //only look at const parameters
    if(arg) {
      std::cout << "ERROR: Only one argument can have a LimitScan binning" << std::endl;
      return 0;
    }
    arg = dynamic_cast<RooRealVar*>(a);
  }

  PValueFunction pvals(w,poi,CLs,_compatibilityFunction,alpha,sigma,(arg)?arg->GetName():0);

  
  
  ROOT::Math::WrappedFunction<PValueFunction&> wf(pvals);
  
  double out = 0;
  
  int currBin = (arg) ? arg->getBin("LimitScan") : 0;
  int numBins = (arg) ? arg->numBins("LimitScan") : 1;

  int exceptionCount = 0;

  if (sigma) {
      TRooFit::msg().Info("FindExpectedLimit","nSigma = %g",*sigma);
  }

  TGraph* scan_graph;
  if(arg) { 
    TRooFit::msg().Info("FindLimit","Scanning %s at %d points",arg->GetName(),numBins);
    scan_graph = new TGraph;
  }

  for(int i=0;i<numBins;i++) {
    if(arg) arg->setBin(i,"LimitScan");
    
    ROOT::Math::BrentRootFinder brf;
    brf.SetNpx(3);
    brf.SetFunction( wf, w->var(poi)->getMin("LimitScan"), w->var(poi)->getMax("LimitScan") );
    if(pvals.unconditionalFit) delete pvals.unconditionalFit;
    pvals.unconditionalFit = 0;

    double result = 0;
    try {
      //brf.Solve(100,0,1e-2); result = brf.Root();
      result = customRootFinder(pvals, w->var(poi)->getMin("LimitScan"), w->var(poi)->getMax("LimitScan") );
    } catch(double badX) {
      pvals.reset();

      if( fabs(w->var(poi)->getMin("LimitScan") - badX) < fabs(w->var(poi)->getMax("LimitScan") - badX) && badX+(w->var(poi)->getMax("LimitScan")-badX)*0.05  <= 0 ) {
        w->var(poi)->setMin("LimitScan",badX+(w->var(poi)->getMax("LimitScan")-badX)*0.05 );
      } else {
        w->var(poi)->setMax("LimitScan",badX+(w->var(poi)->getMin("LimitScan")-badX)*0.05 );
      }
      w->loadFit(0); //reset to nominal values
      TRooFit::msg().Warning("FindLimit","Bad p-values detected @ %g ... restricting LimitScan range to [%g,%g]",badX, w->var(poi)->getMin("LimitScan"), w->var(poi)->getMax("LimitScan") );
      i--;
      exceptionCount++;
      if(exceptionCount==3) {
        TRooFit::msg().Error("FindLimit","Too many bad fits, please try using a more sensible LimitScan range");
        throw badX; //stop after moving the range too many times
      }
      continue;
    }
    
    if(i==currBin) out = result;
    if(arg) {
      TRooFit::msg().Info("FindLimit","%s=%g limit: %g",arg->GetName(),arg->getVal(),result);
      scan_graph->SetPoint(scan_graph->GetN(),arg->getVal(),result);
    }
  }

  if(arg) arg->setBin(currBin,"LimitScan");
  
  if(TRooFit::GetDebugFile()) {
    TDirectory* tmpDir = gDirectory;
    TRooFit::GetDebugFile()->cd();
    std::string extra = "";
    if (sigma) {
        extra = Form("_expected%g",*sigma);
    }
    extra += "_";
    extra += w->data()->GetName();
    if(arg) {
      pvals.nullPVals2.Write(Form("nullPVals%s_%s_%s",extra.c_str(),arg->GetName(),poi));
      pvals.altPVals2.Write(Form("altPVals%s_%s_%s",extra.c_str(),poi,arg->GetName()));
      if(CLs) pvals.clsPVals2.Write(Form("clsPVals%s_%s_%s",extra.c_str(),arg->GetName(),poi));
      scan_graph->Write(Form("limit%s_%s_scan_%s",extra.c_str(),poi,arg->GetName()));
      delete scan_graph;
    } else {
      pvals.nullPVals.Write(Form("nullPVals%s_%s",extra.c_str(),poi));
      pvals.altPVals.Write(Form("altPVals%s_%s",extra.c_str(),poi));
      if(CLs) pvals.clsPVals.Write(Form("clsPVals%s_%s",extra.c_str(),poi));
    }
    tmpDir->cd();
  }


  return out;
}


std::vector<double> TRooFit::Asymptotics::FindExpectedLimits(TRooWorkspace* w, const char* poi, bool CLs, std::vector<double>&& sigmas, bool (*_compatibilityFunction)(double mu, double mu_hat), double alpha) {
  std::vector<double> out;
  for(auto sigma : sigmas) {
    out.push_back( FindLimit(w,poi,CLs,_compatibilityFunction,alpha,&sigma) );
  }
  return out;
}
